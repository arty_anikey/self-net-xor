#!/usr/bin/env python

from random import random, uniform
from absl import app, flags

from nets.v1 import draw, NeuralNetwork, relu, lin, sigmoid
from utils import learn_measured
from problems.xor import learning_set

FLAGS = flags.FLAGS


def main(_):
    network = NeuralNetwork(
        inputs_count=2,
        structure=[(5, sigmoid), (1, sigmoid)],
        init_weight_function=lambda: uniform(-1.0, 1.0),
        weight_update_modifier=1.0,
    )
    # TASK: try the following network configuration:
    #
    #  structure=[(5, relu), (1, lin)],
    #  init_weight_function=lambda: random(),
    #  weight_update_modifier=0.05,
    learn_measured(network, learning_set, FLAGS, draw)

    for state in ((0.0, 0.0), (1.0, 0.0), (0.0, 1.0), (1.0, 1.0)):
        print(state, network.predict(state))


if __name__ == "__main__":
    flags.DEFINE_integer("epochs_count", 10000, "Number of epochs.")
    flags.DEFINE_boolean("visualize", False, "Defines whether to write visualization text file.")

    app.run(main)
