#!/usr/bin/env python

from absl import app, flags

from nets.v1 import draw, NeuralNetwork, relu
from utils import learn_measured
from problems.addition import learning_set

FLAGS = flags.FLAGS


def main(_):
    network = NeuralNetwork(
        inputs_count=2,
        structure=[(5, relu), (1, relu)],
        init_weight_function=lambda: 1.0,
        weight_update_modifier=1.0,
    )

    learn_measured(network, learning_set, FLAGS, draw)

    for state in ((0.6, 0.3), (0.7, 0.1), (12, 15), (-2, -3)):
        # TASK: update the network to be able to add negative numbers
        #  (hint: see next task). Why did the change work?
        # TASK: play with the network structure
        #  try changing number of inner layers, number of neurons in layers
        #  and layer activation functions
        print(state, network.predict(state))


if __name__ == "__main__":
    flags.DEFINE_integer("epochs_count", 1000, "Number of epochs.")
    flags.DEFINE_boolean("visualize", False, "Defines whether to write visualization text file.")

    app.run(main)
