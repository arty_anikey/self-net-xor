#!/usr/bin/env python

from random import random
from absl import app, flags
from tqdm import trange

from nets.v1 import NeuralNetwork, lin, relu
from problems.mnist import learning_set, testing_set

FLAGS = flags.FLAGS


def main(_):
    network = NeuralNetwork(
        inputs_count=28*28,
        structure=[(40, relu), (10, lin)],
        init_weight_function=lambda: 0.2 * random() - 0.1,
        weight_update_modifier=0.005,
    )

    for i in trange(len(learning_set)):
        state, expected_output = learning_set[i]
        network.learn(state, expected_output)

    for i, state in enumerate(testing_set[0]):
        output = network.predict(state)
        biggest_value = max(output)
        predicted_value = output.index(biggest_value)
        print("Expected: {} Predicted: {}".format(testing_set[1][i], predicted_value))


if __name__ == "__main__":
    flags.DEFINE_integer("epochs_count", 1, "Number of epochs.")
    flags.DEFINE_boolean("visualize", False, "Defines whether to write visualization text file.")

    app.run(main)
