import numpy as np
from tqdm import trange
from utils.mnist_loader import read_data_set

x_train, y_train = read_data_set("./data/mnist/train-images-idx3-ubyte", "./data/mnist/train-labels-idx1-ubyte")
x_test, y_test = read_data_set("./data/mnist/t10k-images-idx3-ubyte", "./data/mnist/t10k-labels-idx1-ubyte")

images, labels = x_train[0:1000] / 255, y_train[0:1000]

one_hot_labels = np.zeros((len(labels), 10))

for i, l in enumerate(labels):
    one_hot_labels[i][l] = 1

labels = one_hot_labels

test_images = x_test / 255
test_labels = np.zeros((len(y_test), 10))

for i, l in enumerate(y_test):
    test_labels[i][l] = 1

np.random.seed(1)

relu = lambda x: (x >= 0) * x
relu_deriv = lambda x: x >= 0

alpha = 0.005
iterations = 350
hidden_size = 40
pixels_per_image = 784
num_labels = 10

weights_0_1 = 0.2 * np.random.random((pixels_per_image, hidden_size)) - 0.1
weights_1_2 = 0.2 * np.random.random((hidden_size, num_labels)) - 0.1

for j in trange(iterations):
    # error, correct_count = (0.0, 0)

    for i in range(len(images)):
        layer_0 = images[i:i+1]
        layer_1 = relu(np.dot(layer_0, weights_0_1))
        layer_2 = np.dot(layer_1, weights_1_2)

        # error += np.sum((labels[i:i+1] - layer_2) ** 2)
        # correct_count += int(np.argmax(layer_2) == np.argmax(labels[i:i+1]))

        layer_2_error = (labels[i:i + 1] - layer_2)
        a = layer_2_error
        b = weights_1_2.T
        c = a.dot(b)
        layer_1_error = c * relu_deriv(layer_1)

        # >> > import numpy as np
        # >> > a = np.array([[ 1,       2 ]])
        # >> > b = np.array([[2, 3], [3, 4]])
        # >> > c = a.dot(b)
        # >> > c
        # array([[ 8, 11]])
        #
        # The error of neuron in N-1 layer equals sum of errors of all connected neurons in N layer
        # multiplied by the weight of those respective "connections" (synapses).

        d = layer_1.T
        e = layer_2_error
        weights_1_2 += alpha * d.dot(e)

        # >> > d = np.array([[1], [2]])
        # >> > e = np.array([[3, 4]])
        # >> > d.dot(e)
        # array([[3, 4],
        #        [6, 8]])
        #
        # Update to the weight equals signal coming through that synapse to the neuron multiplied by
        # this neuron error.

        weights_0_1 += alpha * layer_0.T.dot(layer_1_error)

for i in range(10):
    layer_0 = test_images[i:i + 1]
    layer_1 = relu(np.dot(layer_0, weights_0_1))
    layer_2 = np.dot(layer_1, weights_1_2)

    output = list(layer_2[0])
    biggest_value = max(output)
    predicted_value = output.index(biggest_value)
    print("Expected {} Predicted {}".format(y_test[i], predicted_value))
