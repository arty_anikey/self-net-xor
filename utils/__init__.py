from time import time
from tqdm import trange


def learn_measured(network, learning_set, flags, draw_func):
    start_time = time()

    print("Running training...")

    for _ in trange(flags.epochs_count):
        for state, expected_output in learning_set:
            network.learn(state, expected_output)

            if flags.visualize:
                draw_func(network)

    print("Complete. Elapsed time: {}".format(time() - start_time))
