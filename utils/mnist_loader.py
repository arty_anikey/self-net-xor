#
# This is a sample Notebook to demonstrate how to read "MNIST Dataset"
#
import numpy
import struct
from array import array


def read_data_set(images_filepath, labels_filepath):
    labels = []

    with open(labels_filepath, 'rb') as file:
        magic, size = struct.unpack(">II", file.read(8))
        if magic != 2049:
            raise ValueError('Magic number mismatch, expected 2049, got {}'.format(magic))
        labels = array("B", file.read())

    with open(images_filepath, 'rb') as file:
        magic, size, rows, cols = struct.unpack(">IIII", file.read(16))
        if magic != 2051:
            raise ValueError('Magic number mismatch, expected 2051, got {}'.format(magic))
        image_data = array("B", file.read())

    images = []
    for i in range(size):
        img = numpy.array(image_data[i * rows * cols:(i + 1) * rows * cols])
        images.append(img)

    return numpy.array(images), labels
