from nets.activation_functions import DERIVATIVES


class NeuralNetwork:
    """
        Fully connected NN with configurable structure
        (one can configure number of hidden layers and their activation functions)
        and weights init function. NN is always fully connected.
    """
    def __init__(self, inputs_count, structure, init_weight_function, weight_update_modifier):
        self.layers = [[InputNeuron(None, None, None) for _ in range(inputs_count)]]

        for layer_properties in structure:
            self.layers.append([Neuron(layer_properties[1], init_weight_function, weight_update_modifier) for _ in range(layer_properties[0])])

        for idx, layer in enumerate(self.layers[1:], 1):
            for neuron in layer:
                neuron.connect(self.layers[idx - 1])

    def _forward_propagate(self, state):
        for idx, value in enumerate(state):
            self.layers[0][idx].value = value

        #
        # State forward propagation.
        #
        for layer in self.layers[1:]:
            for neuron in layer:
                neuron.activate()

    def learn(self, state, expected):
        self._forward_propagate(state)

        for idx, value in enumerate(expected):
            neuron = self.layers[-1][idx]
            neuron.learn(expected[idx] - neuron.value)

    def predict(self, state):
        self._forward_propagate(state)

        return [neuron.value for neuron in self.layers[-1]]


class Synapse:
    def __init__(self, neuron, weight):
        self.neuron = neuron
        self.weight = weight


class Neuron:
    def __init__(self, activation_function, init_weight_function, weight_update_modifier):
        self.error = 0.0
        self.value = 0.0
        self.synapses = []
        self.activation_function = activation_function
        self._init_weight_function = init_weight_function
        self._weight_update_modifier = weight_update_modifier

    def connect(self, input_neurons):
        for neuron in input_neurons:
            self.synapses.append(
                Synapse(neuron, self._init_weight_function()),
            )

    def activate(self):
        self.value = self.activation_function(
            sum([synapse.neuron.value * synapse.weight for synapse in self.synapses])
        )

    def learn(self, error):
        self.error = error

        for synapse in self.synapses:
            descending_error = error * DERIVATIVES[self.activation_function](self.value)

            synapse.neuron.learn(descending_error * synapse.weight)

            synapse.weight += self._weight_update_modifier * synapse.neuron.value * descending_error


class InputNeuron:
    def __init__(self, _activation_function, _init_weight_function, _weight_update_modifier):
        self.value = 0.0

    def activate(self):
        pass

    def learn(self, _):
        pass

    def connect(self, _):
        pass


def draw(network):
    visual_file = open("network.visual", "w")

    visual_file.seek(0)
    visual_file.write("          ".join([str(neuron.value) for neuron in network.layers[0]]) + "\n")
    visual_file.write("|\n")

    for layer_idx, _ in enumerate(network.layers[1:], 1):
        visual_file.write("          ".join(["========="] * len(network.layers[layer_idx])) + "\n")

        for neuron_idx, _ in enumerate(network.layers[layer_idx - 1]):
            visual_file.write("          ".join(
                ["{: f}".format(neuron.synapses[neuron_idx].weight) for neuron in network.layers[layer_idx]]
            ) + "\n")
        visual_file.write("          ".join(["--error--"] * len(network.layers[layer_idx])) + "\n")
        visual_file.write("          ".join(
            ["{: f}".format(neuron.error) for neuron in network.layers[layer_idx]]
        ) + "\n")
        visual_file.write("          ".join(["-output--"] * len(network.layers[layer_idx])) + "\n")
        visual_file.write("          ".join(
            ["{: f}".format(neuron.value) for neuron in network.layers[layer_idx]]
        ) + "\n")
        visual_file.write("          ".join(["========="] * len(network.layers[layer_idx])) + "\n")
        visual_file.write("|\n")
    visual_file.close()
