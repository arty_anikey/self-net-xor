#!/usr/bin/env python

from nets.activation_functions import lin, relu, DERIVATIVES
from random import random


class NeuralNetwork:
    def __init__(self, structure):
        self._layers = []

        for idx, neurons_count in enumerate(structure):
            if idx == 0:
                layer = [StateNeuron() for _ in range(neurons_count)]
            elif idx == len(structure) - 1:
                layer = [PredictionNeuron(lin) for _ in range(neurons_count)]
            else:
                layer = [HiddenNeuron(i, relu) for i in range(neurons_count)]

            self._layers.append(layer)

        for idx, layer in enumerate(self._layers):
            for neuron in layer:
                if idx == 0:
                    neuron.connect([], self._layers[idx + 1])
                elif idx == len(self._layers) - 1:
                    neuron.connect(self._layers[idx - 1], [])
                else:
                    neuron.connect(self._layers[idx - 1], self._layers[idx + 1])

    def _forward_propagate(self, state):
        for idx, value in enumerate(state):
            self._layers[0][idx].output = value

        #
        # State forward propagation.
        #
        for layer in self._layers[1:]:
            for neuron in layer:
                neuron.calculate_output()

    def learn(self, state, solution):
        self._forward_propagate(state)

        for idx, value in enumerate(solution):
            self._layers[-1][idx].expected = solution[idx]

        #
        # Error backward propagation.
        #
        for layer in reversed(self._layers[1:]):
            for neuron in layer:
                neuron.calculate_error()

        #
        # Weights update.
        #
        for layer in self._layers[1:]:
            for neuron in layer:
                neuron.update_weights()

        return [neuron.output for neuron in self._layers[-1]]

    def predict(self, state):
        self._forward_propagate(state)

        return [neuron.output for neuron in self._layers[-1]]


class Neuron:
    def __init__(self):
        self._output = 0.0
        self._output_neurons = None
        self._input_neurons = None

    def connect(self, input_neurons, output_neurons):
        self._input_neurons = input_neurons
        self._output_neurons = output_neurons

    @property
    def output(self):
        return self._output


class StateNeuron(Neuron):
    @property
    def output(self):
        return super(StateNeuron, self).output

    @output.setter
    def output(self, new_output):
        self._output = new_output


class NeuronCore(Neuron):
    def __init__(self, activation_function):
        super(NeuronCore, self).__init__()

        self._weights = []
        self._error = 0.0

        self._activation_function = activation_function

    @property
    def error(self):
        return self._error

    @property
    def weights(self):
        return self._weights

    def connect(self, input_neurons, output_neurons):
        super(NeuronCore, self).connect(input_neurons, output_neurons)

        for _ in range(len(input_neurons)):
            #
            # These are considered to be the best initialization values.
            #
            self._weights.append(0.2 * random() - 0.1)

    def calculate_output(self):
        weighted_input = 0.0

        for idx, neuron in enumerate(self._input_neurons):
            weighted_input += neuron.output * self._weights[idx]

        self._output = self._activation_function(weighted_input)

    def update_weights(self):
        for idx, _ in enumerate(self._weights):
            self._weights[idx] += 0.005 * self._input_neurons[idx].output \
                * self._error \
                * DERIVATIVES[self._activation_function](self._output)


class PredictionNeuron(NeuronCore):
    def __init__(self, activation_function):
        super(PredictionNeuron, self).__init__(activation_function)

        self.expected = 0.0

    def calculate_error(self):
        self._error = self.expected - self._output


class HiddenNeuron(NeuronCore):
    def __init__(self, idx_in_layer, activation_function):
        super(HiddenNeuron, self).__init__(activation_function)

        self._idx = idx_in_layer

    def calculate_error(self):
        self._error = 0.0

        for neuron in self._output_neurons:
            self._error += neuron.error * neuron.weights[self._idx]
