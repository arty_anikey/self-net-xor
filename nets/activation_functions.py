from math import exp


def sigmoid(weighted_input):
    return 1.0 / (1.0 + exp(-weighted_input))


def hyper_tang(weighted_input):
    return (exp(weighted_input) - exp(-weighted_input)) / (exp(weighted_input) + exp(-weighted_input))


def relu(weighted_input):
    return (weighted_input > 0) * weighted_input


def lin(weighted_input):
    return weighted_input


DERIVATIVES = {
    sigmoid: lambda output: output * (1.0 - output),
    hyper_tang: lambda output: 1 - pow(output, 2),
    relu: lambda output: output > 0,
    lin: lambda output: 1,
}
