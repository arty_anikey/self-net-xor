from utils.mnist_loader import read_data_set

x_train, y_train = read_data_set("./data/mnist/train-images-idx3-ubyte", "./data/mnist/train-labels-idx1-ubyte")
x_test, y_test = read_data_set("./data/mnist/t10k-images-idx3-ubyte", "./data/mnist/t10k-labels-idx1-ubyte")

learning_set = []

for i in range(10000):
    output = [0]*10
    output[y_train[i]] = 1.0
    learning_set.append((x_train[i] / 255.0, output,))

testing_set = [[], []]

for i in range(10):
    testing_set[0].append(x_test[i])
    testing_set[1].append(y_test[i])
